package ru.t1.malyugin.tm.repository;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IUserOwnerRepository;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnerRepository<M> {

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> modelsToRemove = findAll(userId);
        removeAll(modelsToRemove);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return findAll(userId).size();
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    public M remove(@NotNull final String userId, @NotNull final M model) {
        if (StringUtils.equals(userId, model.getUserId())) return remove(model);
        return null;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return models.values().stream()
                .filter(m -> StringUtils.equals(userId, m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        return findAll(userId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return findAll(userId).stream()
                .filter(m -> StringUtils.equals(id, m.getId()))
                .findAny().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

}