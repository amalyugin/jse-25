package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.IProjectTaskService;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;
import ru.t1.malyugin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.malyugin.tm.exception.field.TaskIdEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (projectRepository.findOneById(userId.trim(), projectId.trim()) == null)
            throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId.trim(), taskId.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId.trim());
        return task;
    }

    @Nullable
    @Override
    public Project removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        @Nullable final Project project = projectRepository.findOneById(userId.trim(), projectId.trim());
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId.trim(), projectId.trim());
        for (@NotNull final Task task : tasks) taskRepository.remove(userId.trim(), task);
        return projectRepository.remove(userId, project);
    }

    @Override
    public @Nullable Project removeProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= projectRepository.getSize(userId))
            throw new IndexIncorrectException();
        @Nullable final Project project = projectRepository.findOneByIndex(userId.trim(), index);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId.trim(), project.getId());
        for (@NotNull final Task task : tasks) taskRepository.remove(userId.trim(), task);
        return projectRepository.remove(userId, project);
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (projectRepository.findOneById(userId.trim(), projectId.trim()) == null)
            throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId.trim(), taskId.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

}