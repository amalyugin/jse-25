package ru.t1.malyugin.tm.util;

import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextInteger() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final RuntimeException e) {
            throw new NumberIncorrectException(value, e);
        }
    }

    @Nullable
    static Integer nextIntegerSafe() {
        @NotNull final String value = nextLine();
        if (NumberUtils.isParsable(value)) return Integer.parseInt(value);
        return null;
    }

}