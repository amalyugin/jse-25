package ru.t1.malyugin.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

    public IndexIncorrectException(String value) {
        super("Error! This index '" + value + "' is incorrect...");
    }

}