package ru.t1.malyugin.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;

public enum Status {

    NOT_STARTED("Not Started"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Completed");

    @NotNull
    @Getter
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String renderValuesList() {
        @NotNull final StringBuilder result = new StringBuilder();
        final int size = Status.values().length;
        for (int i = 0; i < size; i++) {
            result.append(String.format("%d - %s, ", i, Status.values()[i].displayName));
        }
        return result.toString();
    }

    @NotNull
    public static Status getStatusByIndex(final Integer index) {
        if (index == null || index < 0 || index >= Status.values().length) throw new IndexIncorrectException();
        return Status.values()[index];
    }

}