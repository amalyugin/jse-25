package ru.t1.malyugin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-show";

    @NotNull
    private static final String DESCRIPTION = "show current user profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER SHOW PROFILE]");
        @NotNull final User user = getAuthService().getUser();
        renderProfile(user);
    }

}